<?php

return [

    'password' => [
        'table' => 'password_resets',
        'expire' => 60,
    ],

    'role' => [
        'multi' => false,
        'table' => 'roles',
        'pivot' => 'bastion_role',
        'model' => null,
    ],

    'permission' => [
        'table' => 'permissions',
        'pivot' => 'permission_role',
        'model' => null,
    ],

    'throttle' => [
        'attempts' => 5,
        'timeout' => 60,
    ],

    'auth' => [
        'user' => [

            'driver' => 'eloquent',
            'model' => \App\Models\User::class,

            'email' => 'emails.password',
            'provider' => [
                'query' => [

                ]
            ],
        ],
    ],
];