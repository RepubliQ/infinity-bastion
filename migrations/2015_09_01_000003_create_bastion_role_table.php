<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBastionRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bastion_role', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->index();

            // Comment if role model is multi
            $table->integer('user_id')->unsigned()->index();

            // Uncomment if role model is multi
            //$table->integer('model_id')->unsigned()->index();
            //$table->string('model_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bastion_role');
    }
}