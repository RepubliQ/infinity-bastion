<?php

namespace Infinity\Bastion\Role;

trait RoleFunctionalityTrait
{
    public static function bootRoleFunctionality()
    {
        static::deleting(function ($role) {
            if (!(bool)$role['deletable']) {
                return false;
            }
        });

        static::deleted(function ($role) {
            if (!method_exists($role, 'bootSoftDeletes')) {
                if (method_exists($role, 'bootHasPermissions')) {
                    $role->permissions()->sync([]);
                }

                \DB::table(config('bastion.role.pivot'))->where('role_id', $role['id'])->delete();
            }
        });
    }
}