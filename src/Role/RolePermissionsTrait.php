<?php

namespace Infinity\Bastion\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Infinity\Bastion\Contracts\Role\Permissions as HasPermissionsContract;

trait RolePermissionsTrait
{
    /**
     * A list of permissions.
     *
     * @type array
     */
    protected $permissions;

    public static function bootHasPermissions()
    {
        static::deleting(function (HasPermissionsContract $role) {
            if (!method_exists($role, 'bootSoftDeletes')) {
                $role->permissions()->sync([]);
            }
        });
    }

    /**
     * User permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany|Builder
     */
    public function permissions()
    {
        return $this->belongsToMany(config('bastion.permission.model'), config('bastion.permission.pivot'));
    }

    /**
     * Check if user has permissions.
     *
     * @param string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function hasPermission($permission, $requireAll = false)
    {
        if (is_array($permission)) {
            $satisfied = 0;
            $permissions = $this->loadPermissions();

            foreach ($permission as $permissionName) {
                $satisfied += in_array($permissionName, $permissions, true) ? 1 : 0;
            }

            if ($requireAll && $satisfied !== count($permission)) {
                return false;
            } else {
                return $satisfied > 0;
            }
        } else {
            return in_array($permission, $this->loadPermissions(), true);
        }
    }

    /**
     * Add permission or permissions to user.
     *
     * @param Model|array $permission
     */
    public function addPermission($permission)
    {
        $this->permissions()->attach($permission);
    }

    /**
     * Remove permission or permissions from user.
     *
     * @param Model|array $permission
     */
    public function removePermission($permission)
    {
        $this->permissions()->detach($permission);
    }

    /**
     * Return all permissions associated with the user.
     *
     * @return array
     */
    protected function loadPermissions()
    {
        if ($this->permissions === null) {
            if (method_exists($this, 'loadRoles')) {
                $this->loadRoles();
            } else {
                $this->permissions = $this->permissions()->lists('slug');
            }
        }

        return $this->permissions;
    }
}