<?php

namespace Infinity\Bastion\Contracts\Role;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;

interface Permissions
{
    /**
     * User permissions.
     *
     * @return BelongsToMany|Builder
     */
    public function permissions();

    /**
     * Check if user has permissions.
     *
     * @param string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function hasPermission($permission, $requireAll = false);

    /**
     * Add permission or permissions to user.
     *
     * @param Model|array $permission
     */
    public function addPermission($permission);

    /**
     * Remove permission or permissions from user.
     *
     * @param Model|array $permission
     */
    public function removePermission($permission);
}