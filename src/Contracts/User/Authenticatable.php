<?php

namespace Infinity\Bastion\Contracts\User;

use Infinity\Bastion\Base\Bastion;

interface Authenticatable
{
    /**
     * Return bastion configuration name.
     *
     * @return string
     */
    public static function getBastionName();

    /**
     * Return class name.
     *
     * @return
     */
    public static function getClassName();

    /**
     * Return bastion instance.
     *
     * @return Bastion
     */
    public static function auth();

    /**
     * Get user key.
     *
     * @return mixed
     */
    public function getKey();

    /**
     * Return the user token column name.
     *
     * @return string
     */
    public function getRememberTokenName();

    /**
     * Return the user token column name.
     *
     * @return string
     */
    public function getRememberToken();

    /**
     * Set user remember token.
     *
     * @param $token
     * @return mixed
     */
    public function setRememberToken($token);

    /**
     * Return user identifier value.
     *
     * @return string
     */
    public function getUserIdentifier();

    /**
     * Return user identifier value.
     *
     * @return string
     */
    public function getUserIdentifierName();

    /**
     * Return user password value.
     *
     * @return string
     */
    public function getUserPassword();

    /**
     * Return user password value.
     *
     * @return string
     */
    public function getUserPasswordName();
}