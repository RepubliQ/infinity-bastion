<?php

namespace Infinity\Bastion\Contracts\User;

interface CanResetPassword
{
    /**
     * Return bastion configuration name.
     *
     * @return string
     */
    public static function getBastionName();

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset();

    /**
     * Update user password.
     *
     * @param string $newPassword
     */
    public function updatePassword($newPassword);

    /**
     * Return user identifier value.
     *
     * @return string
     */
    public function getUserIdentifierName();

    /**
     * Return user password value.
     *
     * @return string
     */
    public function getUserPasswordName();
}