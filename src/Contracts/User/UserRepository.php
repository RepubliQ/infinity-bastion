<?php

namespace Infinity\Bastion\Contracts\User;

interface UserRepository
{
    /**
     * Retrieve user by their identifier.
     *
     * @param $id
     * @return Authenticatable|null
     */
    public function findById($id);

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param $id
     * @param string $token
     * @return Authenticatable|null|static
     */
    public function findByRememberToken($id, $token);

    /**
     * Find user by credentials.
     *
     * @param $credentials
     * @return Authenticatable|null
     */
    public function findByCredentials($credentials);

    /**
     * Find user by custom values.
     *
     * @param string $identifier
     * @param array $columns
     * @return Authenticatable|null
     */
    public function findByIdentifier($identifier, array $columns = ['*']);

    /**
     * Validate a user against the given credentials.
     *
     * @param $testPassword
     * @param $userPassword
     * @return bool
     */
    public function validPassword($testPassword, $userPassword);

    /**
     * Update user remember token.
     *
     * @param Authenticatable $user
     * @param $token
     */
    public function updateRememberToken(Authenticatable $user, $token);

    /**
     * Apply model requirements.
     *
     * @param $target
     * @return mixed
     */
    public function applyRequirements($target);
}