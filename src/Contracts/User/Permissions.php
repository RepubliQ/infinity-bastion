<?php

namespace Infinity\Bastion\Contracts\User;

interface Permissions
{
    /**
     * Check if user has permissions.
     *
     * @param string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function can($permission, $requireAll = false);
}