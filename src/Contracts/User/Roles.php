<?php

namespace Infinity\Bastion\Contracts\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Query\Builder;

interface Roles
{
    /**
     * User roles.
     *
     * @return BelongsToMany|Builder
     */
    public function roles();

    /**
     * Check if user has role.
     *
     * @param string|array $name
     * @param bool $requireAll
     * @return bool
     */
    public function hasRole($name, $requireAll = false);

    /**
     * Add role or roles to user.
     *
     * @param Model|array $role
     */
    public function addRole($role);

    /**
     * Remove role or roles from user.
     *
     * @param Model|array $role
     */
    public function removeRole($role);

    /**
     * Return whether the user has super role.
     *
     * @return bool
     */
    public function superRole();
}