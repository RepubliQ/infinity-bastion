<?php

namespace Infinity\Bastion\Contracts\Reset;

use Infinity\Bastion\Contracts\User\CanResetPassword;

interface TokenRepository
{
    /**
     * Create a new token.
     *
     * @param CanResetPassword $user
     * @return string
     */
    public function create(CanResetPassword $user);

    /**
     * Determine if a token record exists and is valid.
     *
     * @param CanResetPassword $user
     * @param string $token
     * @return bool
     */
    public function exists(CanResetPassword $user, $token);

    /**
     * Delete a token record.
     *
     * @param string $token
     */
    public function delete($token);

    /**
     * Delete expired tokens.
     */
    public function deleteExpired();
}