<?php

namespace Infinity\Bastion\Contracts\Reset;

use Illuminate\Contracts\Validation\Validator;
use Infinity\Bastion\Contracts\User\CanResetPassword;

interface PasswordBroker
{
    /**
     * Send a password reset link to a user.
     *
     * @param CanResetPassword|string $user
     * @param \Closure|null $callback
     * @return string
     */
    public function sendResetLink($user, \Closure $callback = null);

    /**
     * Reset the password for the given token.
     *
     * @param CanResetPassword|string $user
     * @param array $data
     * @param Validator|\Closure|null $validator
     * @return mixed
     */
    public function reset($user, array $data, $validator = null);
}