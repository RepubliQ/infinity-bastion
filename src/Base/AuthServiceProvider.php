<?php

namespace Infinity\Bastion\Base;

use Illuminate\Support\ServiceProvider;
use Infinity\Bastion\User\Providers\EloquentRepository;
use Welkin\Bastion\Exceptions\InvalidModelProvided;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__ . '/../../config' => config_path()], 'config');
        $this->publishes([__DIR__ . '/../../migrations' => database_path('migrations')]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBastion();

        $this->registerEloquentDriver();
    }

    /**
     * Register Bastion service.
     */
    private function registerBastion()
    {
        $this->app->singleton('auth', function ($app) {
            return new AuthManager($app);
        });
    }

    /**
     * Register eloquent driver.
     */
    private function registerEloquentDriver()
    {
        $this->app['auth']->extend('eloquent', function ($app, array $config, $name) {

            if (!isset($config['model']) || !class_exists($config['model'])) {
                throw new InvalidModelProvided('Missing model property or class doesn\'t exist.');
            }

            $settings = [];
            if (isset($config['provider'])) {
                $settings = $config['provider'];
                unset($config['provider']);
            }

            $provider = new EloquentRepository($app['hash'], $config['model'], $settings);

            return new Bastion($provider, $app['session.store'], $config, $name);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['auth'];
    }
}