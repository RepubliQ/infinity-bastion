<?php

namespace Infinity\Bastion\Base;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Cookie\CookieJar;
use Illuminate\Session\SessionInterface;
use Illuminate\Http\Request as LaravelRequest;
use Illuminate\Support\Str;
use Infinity\Bastion\Contracts\Reset\PasswordBroker;
use Infinity\Bastion\Exceptions\InvalidAttempt;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Infinity\Bastion\Contracts\User\Authenticatable;
use Infinity\Bastion\Contracts\User\UserRepository;

/**
 * Class Bastion
 *
 * @method PasswordBroker passwordBroker() return password broker if password reset service is registered.
 * @package Infinity\Bastion\Base
 */
class Bastion
{
    /**
     * Configuration name.
     *
     * @type string
     */
    protected $name;

    /**
     * Reference to auth manager.
     *
     * @type AuthManager
     */
    protected $authManager;

    /**
     * Cached user object
     *
     * @type Authenticatable
     */
    protected $user;

    /**
     * Indicates if the logout method has been called.
     *
     * @type bool
     */
    protected $loggedOut;

    /**
     * User provider
     *
     * @type UserRepository
     */
    protected $provider;

    /**
     * Authentication route requirements.
     *
     * @type null|array
     */
    protected $config;

    /**
     * The session used by the bastion.
     *
     * @type \Symfony\Component\HttpFoundation\Session\SessionInterface
     */
    protected $session;

    /**
     * Indicates if the user was authenticated via a recaller cookie.
     *
     * @type bool
     */
    protected $viaRemember = false;

    /**
     * Indicates if a token user retrieval has been attempted.
     *
     * @type bool
     */
    protected $tokenRetrievalAttempted = false;

    /**
     * The Illuminate cookie creator service.
     *
     * @type \Illuminate\Contracts\Cookie\QueueingFactory
     */
    protected $cookie;

    /**
     * The event dispatcher instance.
     *
     * @type \Illuminate\Contracts\Events\Dispatcher
     */
    protected $events;

    /**
     * The request instance.
     *
     * @type \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * Indicates if the user login failed because it was throttled.
     *
     * @type bool
     */
    protected $throttled = false;

    /**
     * Flow checkpoints. Checkpoint is used to do additional checks for user validation.
     *
     * @type array
     */
    protected $checkpoints = [];

    /**
     * Messages.
     *
     * @type MessageBag
     */
    protected $messages;

    /**
     * Bastion constructor.
     *
     * @param UserRepository $provider
     * @param SessionInterface $session
     * @param array $config
     * @param $name
     */
    public function __construct(UserRepository $provider, SessionInterface $session, array $config, $name) {
        $this->provider = $provider;
        $this->session = $session;
        $this->name = $name;
        $this->config = $config;

        // Make sure the model is booted
        new $this->config['model'];
    }

    /**
     * Get bastion config name.
     *
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param $credentials
     * @param bool $remember
     * @param bool $login
     * @return bool
     */
    public function attempt(array $credentials = [], $login = true, $remember = false)
    {
        $canAttempt = $this->fireAttemptEvent($credentials, $login, $remember);

        if (!$canAttempt) {
            return false;
        }

        $user = $this->provider->findByCredentials($credentials);

        if (!$user) {
            $this->fireLoginFailedEvent($credentials);
        } elseif ($login && $this->fireAttemptUserEvent($user)) {
            $this->login($user, $remember);
        }

        return $user !== null;
    }

    /**
     * Log a user into the application.
     *
     * @param Authenticatable $user
     * @param bool|false $remember
     */
    public function login(Authenticatable $user, $remember = false)
    {
        $this->updateSession($user->getKey());

        if ($remember) {
            $this->createRememberTokenIfDoesntExist($user);
            $this->queueRememberCookie($user);
        }

        $this->fireLoginEvent($user, $remember);

        $this->setUser($user);
    }

    /**
     * Log the given user ID into application.
     *
     * @param $id
     * @param bool $remember
     * @return Authenticatable
     * @throws InvalidAttempt
     */
    public function loginUsingId($id, $remember = false)
    {
        $user = $this->provider->findById($id);

        if ($user === null) {
            throw new InvalidAttempt("Invalid login attempt. User with id {$id} doesn't exist.");
        }

        $this->login($user, $remember);

        return $user;
    }

    /**
     * Login the user into application without session or cookies.
     *
     * @param Authenticatable|int|array $target
     * @return bool
     */
    public function loginOnce($target)
    {
        $user = null;

        if ($target instanceof Authenticatable) {
            $user = $target;
        } elseif (is_array($target)) {
            $user = $this->provider->findByCredentials($target);
        } else {
            $user = $this->provider->findById($target);
        }

        if ($user !== null) {
            $this->setUser($user);
        }

        return !$this->loggedOut;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return null|Authenticatable
     */
    public function user()
    {
        if ($this->loggedOut) {
            return null;
        }

        if ($this->user !== null) {
            return $this->user;
        }

        $id = $this->session->get($this->getSessionKeyName());

        $user = null;

        if ($id !== null) {
            $user = $this->provider->findById($id);
        }

        if ($user === null) {
            $recaller = $this->getSessionRememberKey();

            if ($recaller !== null) {
                $user = $this->findUserByRememberKey($recaller);
            }
        }

        if ($user !== null && !$this->passCheckpoints($user)) {
            return $this->user = null;
        }

        if ($user !== null && isset($recaller)) {
            $this->updateSession($user->getUserIdentifier());
            $this->fireLoginEvent($user, $this->viaRemember);
        }

        return $this->user = $user;
    }

    /**
     * Determine if the current user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        return $this->user() !== null;
    }

    /**
     * Determine if the current user is a guest.
     *
     * @return bool
     */
    public function guest()
    {
        return !$this->check();
    }

    /**
     * Log the user out of the application.
     */
    public function logout()
    {
        $user = $this->user();

        $this->clearSession();

        if ($user !== null) {
            $this->provider->updateRememberToken($user, null);
        }

        if (isset($this->events)) {
            $this->events->fire('auth.logout', [$user]);
        }

        $this->setUser(null);
    }

    /**
     * Get the cookie creator instance used by the guard.
     *
     * @return \Illuminate\Contracts\Cookie\QueueingFactory
     * @throws \RuntimeException
     */
    public function getCookieJar()
    {
        if (!isset($this->cookie)) {
            throw new \RuntimeException('Cookie jar has not been set.');
        }

        return $this->cookie;
    }

    /**
     * Set the cookie creator instance used by the guard.
     *
     * @param \Illuminate\Contracts\Cookie\QueueingFactory|CookieJar $cookie
     */
    public function setCookieJar(CookieJar $cookie)
    {
        $this->cookie = $cookie;
    }

    /**
     * Get the user provider used by the guard.
     *
     * @return UserRepository
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set the user provider used by the guard.
     *
     * @param UserRepository $provider
     */
    public function setProvider(UserRepository $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Get the event dispatcher instance.
     *
     * @return Dispatcher
     */
    public function getDispatcher()
    {
        return $this->events;
    }

    /**
     * Set the event dispatcher instance.
     *
     * @param Dispatcher $events
     */
    public function setDispatcher(Dispatcher $events)
    {
        $this->events = $events;
    }

    /**
     * Get the current request instance.
     *
     * @return LaravelRequest|SymfonyRequest
     */
    public function getRequest()
    {
        return $this->request ?: LaravelRequest::createFromGlobals();
    }

    /**
     * Set the current request instance.
     *
     * @param LaravelRequest $request
     * @return $this
     */
    public function setRequest(LaravelRequest $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get auth manager.
     *
     * @return AuthManager
     */
    public function getAuthManager()
    {
        return $this->authManager;
    }

    /**
     * Set AuthManager.
     *
     * @param AuthManager $authManager
     */
    public function setAuthManager(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }

    /**
     * Set the current user.
     *
     * @param Authenticatable|null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
        $this->loggedOut = $user === null;
    }

    /**
     * Add checkpoint.
     *
     * @param \Closure $checkpoint
     */
    public function addCheckpoint(\Closure $checkpoint)
    {
        $this->checkpoints[] = $checkpoint;
    }

    /**
     * Get messages.
     *
     * @return MessageBag
     */
    public function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = new \Illuminate\Support\MessageBag();
        }

        return $this->messages;
    }

    /**
     * Update the session with the given ID.
     *
     * @param string $id
     */
    protected function updateSession($id)
    {
        $this->session->set($this->getSessionKeyName(), $id);
        $this->session->migrate(true);
    }

    /**
     *
     *
     * @param Authenticatable $user
     * @return bool
     */
    protected function passCheckpoints(Authenticatable $user)
    {
        foreach ($this->checkpoints as $checkpoint) {
            $result = $checkpoint($user, $this->request);

            if ($result !== null && $result !== true) {
                $this->getMessages()->add('checkpoint', $result);
                return false;
            }
        }

        return true;
    }

    /**
     * Get a unique identifier for the auth session value.
     *
     * @return string
     */
    protected function getSessionKeyName()
    {
        return 'bastion_login_' . md5($this->name);
    }

    /**
     * Get the name of the cookie used to store the "recaller".
     *
     * @return string
     */
    protected function getSessionRememberKeyName()
    {
        return 'bastion_remember_' . md5($this->name);
    }

    /**
     * Get the decrypted recaller cookie for the request.
     *
     * @return string|null
     */
    protected function getSessionRememberKey()
    {
        return $this->request->cookies->get($this->getSessionRememberKeyName());
    }

    /**
     * Create a new "remember me" token for the user if one doesn't already exist.
     *
     * @param Authenticatable $user
     */
    protected function createRememberTokenIfDoesntExist(Authenticatable $user)
    {
        if (empty($user->getRememberToken())) {
            $this->refreshRememberToken($user);
        }
    }

    /**
     * Create a "remember me" cookie for a given ID.
     *
     * @param $value
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    protected function createRememberToken($value)
    {
        return $this->getCookieJar()->forever($this->getSessionRememberKeyName(), $value);
    }

    /**
     * Refresh the "remember me" token for the user.
     *
     * @param Authenticatable $user
     */
    protected function refreshRememberToken(Authenticatable $user)
    {
        $this->provider->updateRememberToken($user, Str::random(60));
    }

    /**
     * Queue the recaller cookie into the cookie jar.
     *
     * @param Authenticatable $user
     */
    protected function queueRememberCookie(Authenticatable $user)
    {
        $value = $user->getUserIdentifier() . '|' . $user->getRememberToken();
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->getCookieJar()->queue($this->createRememberToken($value));
    }

    /**
     * Determine if the recaller cookie is in a valid format.
     *
     * @param string $recaller
     * @return bool
     */
    protected function validRememberKey($recaller)
    {
        if (!is_string($recaller) || !strpos($recaller, '|')) {
            return false;
        }

        $segments = explode('|', $recaller);

        return count($segments) === 2 && trim($segments[0]) !== '' && trim($segments[1]) !== '';
    }

    /**
     * Pull a user from the repository by its recaller ID.
     *
     * @param $token
     * @return null|Authenticatable
     */
    protected function findUserByRememberKey($token)
    {
        $user = null;

        if ($this->tokenRetrievalAttempted && $this->validRememberKey($token)) {
            $this->tokenRetrievalAttempted = true;

            list($id, $recaller) = explode('|', $token, 2);

            $this->viaRemember = ($user = $this->provider->findByRememberToken($id, $recaller) !== null);
        }

        return $user;
    }

    /**
     * Fire login failed event.
     *
     * @param $credentials
     * @return bool
     */
    protected function fireLoginFailedEvent($credentials)
    {
        if ($this->events) {
            $result = $this->events->fire('auth.failed', [$this, $this->request, $this->name, $credentials]);

            return $this->eventPassed($result);
        }

        return true;
    }

    /**
     * Fire the login event if the dispatcher is set.
     *
     * @param Authenticatable $user
     * @param bool $remember
     * @return bool
     */
    protected function fireLoginEvent($user, $remember = false)
    {
        if ($this->events) {
            $result = $this->events->fire('auth.login', [$this, $this->request, $this->name, $user, $remember]);

            return $this->eventPassed($result);
        }

        return true;
    }

    /**
     * Fire the attempt event with the arguments.
     *
     * @param array $credentials
     * @param bool $login
     * @param bool $remember
     * @return bool
     */
    protected function fireAttemptEvent(array $credentials, $login = true, $remember = false)
    {
        if ($this->events) {
            $results = $this->events->fire('auth.attempt',
                [$this, $this->request, $this->name, $credentials, $login, $remember]);

            return $this->eventPassed($results);
        }

        return true;
    }

    /**
     * Fire the user attempt event when attempting to login and user found.
     *
     * @param Authenticatable $user
     * @param bool|true $login
     * @param bool|false $remember
     * @return bool
     */
    protected function fireAttemptUserEvent(Authenticatable $user, $login = true, $remember = false)
    {
        if ($this->events) {
            $results = $this->events->fire('auth.attempt.user',
                [$this, $this->request, $this->name, $user, $login, $remember]);

            return $this->eventPassed($results);
        }

        return true;
    }

    /**
     * Remove the user data from the session and cookies.
     */
    protected function clearSession()
    {
        $this->session->remove($this->getSessionKeyName());
        /** @noinspection PhpMethodParametersCountMismatchInspection */
        $this->getCookieJar()->queue($this->getCookieJar()->forget($this->getSessionRememberKeyName()));
    }

    /**
     * Check if event passed.
     *
     * @param $results
     * @return bool
     */
    protected function eventPassed($results)
    {
        return !($results !== null && in_array(false, $results, true));
    }

    /**
     * Call user macro if exists.
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $macro = $this->authManager->getMacro($name);

        if ($macro) {
            array_unshift($arguments, $this);

            return call_user_func_array($macro, $arguments);
        }
    }
}
