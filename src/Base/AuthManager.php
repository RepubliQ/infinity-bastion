<?php

namespace Infinity\Bastion\Base;

use Illuminate\Foundation\Application;
use Infinity\Bastion\Exceptions\InvalidDriverException;
use Infinity\Bastion\Exceptions\NoConfigProvided;

class AuthManager
{
    /**
     * The application instance.
     *
     * @var Application
     */
    protected $app;

    /**
     * The array of created instances of drivers.
     *
     * @type array
     */
    protected $drivers = [];

    /**
     * The registered custom driver creators.
     *
     * @var array
     */
    protected $customCreators = [];

    /**
     * Macros to be used with bastion.
     *
     * @type array
     */
    protected $macros = [];

    /**
     * AuthManager constructor.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Register custom driver provider.
     *
     * @param $driver
     * @param \Closure $callable
     */
    public function extend($driver, \Closure $callable)
    {
        $this->customCreators[$driver] = $callable;
    }

    /**
     * Get a bastion instance.
     *
     * @param $authName
     * @return Bastion
     * @throws \Welkin\Bastion\Exceptions\InvalidDriverException
     */
    public function get($authName)
    {
        if (!isset($this->drivers[$authName])) {
            $this->drivers[$authName] = $this->createDriver($authName);
        }

        return $this->drivers[$authName];
    }

    /**
     * Get all of the created "drivers".
     *
     * @return array
     */
    public function getDrivers()
    {
        return $this->drivers;
    }

    /**
     * Log out all or specific user instances.
     *
     * @param array $instances driver instances that should be logged out.
     */
    public function logout(array $instances = null)
    {
        if ($instances === null) {
            foreach ($this->drivers as $driver) {
                /** @type Bastion $driver */
                $driver->logout();
            }
        } else {
            foreach ($instances as $driver) {
                if (isset($this->drivers[$driver])) {
                    $this->drivers[$driver]->logout();
                }
            }
        }
    }

    /**
     * Add macros to bastion model.
     *
     * @param $name
     * @param \Closure $callback
     */
    public function macro($name, \Closure $callback)
    {
        $this->macros[$name] = $callback;
    }

    /**
     * Get registered macro.
     *
     * @param $macro
     * @return null|\Closure
     */
    public function getMacro($macro)
    {
        if (isset($this->macros[$macro])) {
            return $this->macros[$macro];
        }

        return null;
    }

    /**
     * Get user provider for configuration.
     *
     * @param $authName
     * @param bool $deffer whether to return the provider as a closure for later initiation.
     * @return \Infinity\Bastion\Contracts\User\UserRepository
     */
    public function getUserProvider($authName, $deffer = false)
    {
        if ($deffer) {
            return function() use ($authName) {
                return $this->get($authName)->getProvider();
            };
        }

        return $this->get($authName)->getProvider();
    }

    /**
     * Create bastion instance.
     *
     * @param $authName
     * @return mixed
     * @throws InvalidDriverException
     * @throws NoConfigProvided
     */
    protected function createDriver($authName)
    {
        $config = $this->app['config']["bastion.auth.{$authName}"];

        if ($config === null || !isset($config['driver'])) {
            throw new NoConfigProvided("No bastion config or driver provided for {$authName}.");
        }

        $driver = $config['driver'];

        /** @type $bastion Bastion|null */

        if (isset($this->customCreators[$driver])) {
            $bastion = $this->customCreators[$driver]($this->app, $config, $authName);
        } else {
            throw new InvalidDriverException("No '{$driver}' driver resolver registered.");
        }

        $bastion->setCookieJar($this->app['cookie']);
        $bastion->setDispatcher($this->app['events']);
        $bastion->setRequest($this->app->refresh('request', $bastion, 'setRequest'));
        $bastion->setAuthManager($this);

        return $bastion;
    }

    /**
     * Dynamically call the driver instance.
     *
     * @param string $authName
     * @param array $parameters
     * @return mixed
     */
    public function __call($authName, $parameters)
    {
        return $this->get($authName);
    }
}