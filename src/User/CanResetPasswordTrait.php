<?php

namespace Infinity\Bastion\User;

trait CanResetPasswordTrait
{
    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    /**
     * Update user password.
     *
     * @param string $newPassword
     */
    public function updatePassword($newPassword)
    {
        $this->{$this->getUserPasswordName()} = app('hash')->make($newPassword);
        $this->save();
    }

    /**
     * Send password reset email.
     *
     * @param \Closure|null $callback
     */
    public function sendPasswordResetEmail(\Closure $callback = null)
    {
        self::auth()->passwordBroker()->sendResetLink($this, $callback);
    }

    /**
     * Reset user password.
     *
     * @param array $data
     * @param null $validator
     */
    public function tryResetPassword(array $data, $validator = null)
    {
        return self::auth()->passwordBroker()->reset($this, $data, $validator);
    }
}