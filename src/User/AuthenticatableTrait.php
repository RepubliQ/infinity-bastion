<?php

namespace Infinity\Bastion\User;

use Infinity\Bastion\Base\Bastion;

trait AuthenticatableTrait
{
    /**
     * Return bastion configuration name.
     *
     * @return string
     */
    public static function getBastionName()
    {
        return strtolower(self::getClassName());
    }

    /**
     * Return class name.
     * @return string
     */
    public static function getClassName()
    {
        $name = explode('\\', __CLASS__);
        return array_pop($name);
    }

    /**
     * Return bastion instance.
     *
     * @return Bastion
     */
    public static function auth()
    {
        return bastion(self::getBastionName());
    }

    /**
     * Return the user token column name.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * Return the user token column name.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->{$this->getRememberTokenName()};
    }

    /**
     * Set user remember token.
     *
     * @param $token
     * @return mixed
     */
    public function setRememberToken($token)
    {
        $this->{$this->getRememberTokenName()} = $token;
    }

    /**
     * Return user identifier value.
     *
     * @return string
     */
    public function getUserIdentifier()
    {
        return $this->{$this->getUserIdentifierName()};
    }

    /**
     * Return user identifier value.
     *
     * @return string
     */
    public function getUserIdentifierName()
    {
        return 'email';
    }

    /**
     * Return user password value.
     *
     * @return string
     */
    public function getUserPassword()
    {
        return $this->{$this->getUserPasswordName()};
    }

    /**
     * Return user password value.
     *
     * @return string
     */
    public function getUserPasswordName()
    {
        return 'password';
    }
}