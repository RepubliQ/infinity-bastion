<?php

namespace Infinity\Bastion\User;

trait PermissionAccessTrait
{
    /**
     * A list of permissions.
     *
     * @type array
     */
    protected $permissions;

    /**
     * Check if user has permissions.
     *
     * @param string|array $permission
     * @param bool $requireAll
     * @return bool
     */
    public function can($permission, $requireAll = false)
    {
        if (is_array($permission)) {
            $satisfied = 0;
            $permissions = $this->loadPermissions();

            foreach ($permission as $permissionName) {
                $satisfied += in_array($permissionName, $permissions, true) ? 1 : 0;
            }

            if ($requireAll && $satisfied !== count($permission)) {
                return false;
            } else {
                return $satisfied > 0;
            }
        } else {
            return in_array($permission, $this->loadPermissions(), true);
        }
    }

    /**
     * Return all permissions associated with the user.
     *
     * @return array
     */
    protected function loadPermissions()
    {
        if ($this->permissions === null) {
            if (method_exists($this, 'loadRoles')) {
                $this->loadRoles();
            } elseif (method_exists($this, 'permissions')) {
                $this->permissions = $this->permissions()->lists('slug');
            } else {
                throw new \UnexpectedValueException('User does not have roles or permissions functionality.');
            }
        }

        return $this->permissions;
    }
}