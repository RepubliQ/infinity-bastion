<?php

namespace Infinity\Bastion\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Infinity\Bastion\Contracts\User\Permissions;
use Infinity\Bastion\Contracts\User\Roles as HasRolesContract;

trait RoleAccessTrait
{
    /**
     * A list of roles.
     *
     * @type array
     */
    protected $roles;

    /**
     * Whether the user has super role.
     *
     * @type bool
     */
    protected $superRole = false;

    /**
     * Can access frontend.
     *
     * @type bool
     */
    protected $frontendAccess = false;

    /**
     * Can access backend.
     *
     * @type bool
     */
    protected $backendAccess = false;

    public static function bootHasRoles()
    {
        static::deleting(function (HasRolesContract $user) {
            if (!method_exists(self, 'bootSoftDeletes')) {
                $user->roles()->sync([]);
            }
        });
    }

    /**
     * User roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany|Builder
     */
    public function roles()
    {
        if (config('bastion.role.multi', false)) {
            return $this->morphToMany(config('bastion.role.model'), 'model', config('bastion.role.pivot'));
        } else {
            return $this->belongsToMany(config('bastion.role.model'), config('bastion.role.pivot'));
        }
    }

    /**
     * Check if user has role.
     *
     * @param string|array $name
     * @param bool $requireAll
     * @return bool
     */
    public function hasRole($name, $requireAll = false)
    {
        if (is_array($name)) {
            $satisfied = 0;
            $roles = $this->loadRoles();

            foreach ($name as $roleName) {
                $satisfied += in_array($roleName, $roles, true) ? 1 : 0;
            }

            if ($requireAll && $satisfied !== count($name)) {
                return false;
            } else {
                return $satisfied > 0;
            }
        } else {
            return in_array($name, $this->loadRoles(), true);
        }
    }

    /**
     * Add role or roles to user.
     *
     * @param Model|array $role
     */
    public function addRole($role)
    {
        $this->roles()->attach($role);
    }

    /**
     * Remove role or roles from user.
     *
     * @param Model|array $role
     */
    public function removeRole($role)
    {
        $this->roles()->detach($role);
    }

    /**
     * Can access backend.
     *
     * @return bool
     */
    public function backendAccess()
    {
        if ($this->roles === null) {
            $this->loadRoles();
        }

        return $this->backendAccess;
    }

    /**
     * Can access frontend.
     *
     * @return bool
     */
    public function frontendAccess()
    {
        if ($this->roles === null) {
            $this->loadRoles();
        }

        return $this->frontendAccess;
    }

    /**
     * Return whether the user has super role.
     *
     * @return bool
     */
    public function superRole()
    {
        if ($this->roles === null) {
            $this->loadRoles();
        }

        return $this->superRole;
    }

    /**
     * Return all roles associated with the user.
     *
     * @return array
     */
    protected function loadRoles()
    {
        if ($this->roles === null) {
            $roles = $this->roles()->get(['id', 'slug', 'super', 'frontend', 'backend']);
            $this->roles = $roles->lists('slug')->toArray();

            foreach ($roles as $role) {
                if ($role['super'] === 1) {
                    $this->superRole = true;
                }

                if ($role['backend'] === 1) {
                    $this->backendAccess = true;
                }

                if ($role['frontend'] === 1) {
                    $this->frontendAccess = true;
                }
            }

            if ($this instanceof Permissions) {
                $this->permissions = \DB::table(config('bastion.permission.table'))->whereIn('id', $roles->lists('id'))->groupBy('slug')->lists('slug');
            }
        }

        return $this->roles;
    }
}