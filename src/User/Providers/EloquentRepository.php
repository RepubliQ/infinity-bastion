<?php

namespace Infinity\Bastion\User\Providers;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Infinity\Bastion\Contracts\User\Authenticatable;
use Infinity\Bastion\Contracts\User\UserRepository;

class EloquentRepository implements UserRepository
{
    /**
     * The hasher implementation.
     *
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    protected $hasher;

    /**
     * The Eloquent user model.
     *
     * @var string
     */
    protected $model;

    /**
     * Various settings for user provider.
     *
     * @type array
     */
    protected $settings;

    /**
     * Create a new database user provider.
     *
     * @param Hasher $hasher
     * @param string $model
     * @param array $settings
     */
    public function __construct(Hasher $hasher, $model, array $settings = [])
    {
        $this->model = $model;
        $this->hasher = $hasher;
        $this->settings = $settings;
    }

    /**
     * Retrieve user by their identifier.
     *
     * @param $id
     * @return Authenticatable|null
     */
    public function findById($id)
    {
        return $this->applyRequirements($this->model()->newQuery())->find($id);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param $id
     * @param string $token
     * @return Authenticatable|null|static
     */
    public function findByRememberToken($id, $token)
    {
        $model = $this->model();

        return $this->applyRequirements($model->newQuery())
            ->where($model->getKeyName(), $id)
            ->where($model->getRememberTokenName(), $token)
            ->first();
    }

    /**
     * Find user by credentials.
     *
     * @param $credentials
     * @return Authenticatable|null
     */
    public function findByCredentials($credentials)
    {
        $model = $this->model();

        $sequential = is_numeric(array_keys($credentials)[0]);

        /** @type Model|Authenticatable $model */
        if ($sequential) {
            $model = $this->applyRequirements($model->newQuery())->where($model->getUserIdentifierName(), $credentials[0])->first();
        } else {
            $model = $this->applyRequirements($model->newQuery())->where($model->getUserIdentifierName(), $credentials[$model->getUserIdentifierName()])->first();
        }

        if ($model === null ||
            !$this->validPassword(
                $sequential ? $credentials[0] : $credentials[$model->getUserPasswordName()],
                $model->getUserPassword()
            )
        ) {
            return null;
        }

        return $model;
    }

    /**
     * Find user by custom values.
     *
     * @param string $identifier
     * @param array $columns
     * @return Authenticatable|null
     */
    public function findByIdentifier($identifier, array $columns = ['*'])
    {
        $query = $this->model();

        $query->where($query->getUserIdentifierName(), $identifier);

        return $query->first($columns);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param $testPassword
     * @param $userPassword
     * @return bool
     */
    public function validPassword($testPassword, $userPassword)
    {
        return $this->hasher->check($testPassword, $userPassword);
    }

    /**
     * Update user remember token.
     *
     * @param Authenticatable|Model $user
     * @param $token
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);

        $user->save();
    }

    /**
     * Apply model requirements.
     *
     * @param QueryBuilder|EloquentBuilder $target
     * @return QueryBuilder|EloquentBuilder
     */
    public function applyRequirements($target)
    {
        if (isset($this->settings['query'])) {
            foreach ($this->settings['query'] as $whereParams) {
                call_user_func_array([$target, 'where'], $whereParams);
            }
        }

        return $target;
    }

    /**
     * Get eloquent model.
     *
     * @return EloquentModel|Builder|Authenticatable
     */
    protected function model()
    {
        $class = '\\' . ltrim($this->model, '\\');

        return new $class;
    }
}