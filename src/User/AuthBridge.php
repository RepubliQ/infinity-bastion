<?php

namespace Infinity\Bastion\User;

/**
 * Class AuthBridge
 * @package Infinity\Spring\Bastion\User
 */
trait AuthBridge
{
    /**
     * Log the user in.
     *
     * @param bool $remember
     */
    public function login($remember = false)
    {
        static::auth()->login($this, $remember);
    }

    /**
     * Login the user into application without session or cookies.
     *
     * @return bool
     */
    public function loginOnce()
    {
        static::auth()->loginOnce($this);
    }

    /**
     * Log in a user using id.
     *
     * @param $id
     * @param bool $remember
     * @return $this
     */
    public static function loginUsingId($id, $remember = false)
    {
        return static::auth()->loginUsingId($id, $remember);
    }

    /**
     * Attempt to log the user in using credentials.
     *
     * @param $credentials
     * @param bool $login
     * @param bool $remember
     * @return bool
     */
    public static function attempt($credentials, $login = true, $remember = false)
    {
        return static::auth()->attempt($credentials, $login, $remember);
    }

    /**
     * Check if the user is logged in.
     *
     * @return bool
     */
    public static function loggedIn()
    {
        return static::auth()->check();
    }

    /**
     * Check if user is a guest. Uses global check.
     *
     * @return bool
     */
    public static function guest()
    {
        return static::auth()->guest();
    }

    /**
     * Get logged in user.
     *
     * @return $this
     */
    public static function me()
    {
        return static::auth()->user();
    }
}