<?php

if (!function_exists('bastion')) {
    /**
     * Get Bastion authenticator.
     *
     * @param string|null $name
     * @return \Infinity\Bastion\Base\AuthManager|\Infinity\Bastion\Base\Bastion
     */
    function bastion($name = null)
    {
        if ($name === null) {
            return app('auth');
        }

        return app('auth')->get($name);
    }
}