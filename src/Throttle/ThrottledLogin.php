<?php

namespace Infinity\Bastion\Throttle;

use Illuminate\Http\Request;
use Infinity\Bastion\Base\Bastion;
use Infinity\Bastion\Contracts\User\Authenticatable;

trait ThrottledLogin
{
    public static function bootThrottledLogin()
    {
        app('events')->listen('auth.attempt', function (Bastion $bastion, Request $request, $name) {
            if (self::getBastionName() === $name && $timeout = Throttler::hasTooManyLoginAttempts($name, $request)) {
                $bastion->getMessages()->add('throttle', $timeout);
                return false;
            }
        });

        app('events')->listen('auth.login', function (Bastion $bastion, Request $request, $name) {
            if (self::getBastionName() === $name) {
                Throttler::clearLoginAttempts($name, $request);
            }
        });

        app('events')->listen('auth.failed', function (Bastion $bastion, Request $request, $name) {
            if (self::getBastionName() === $name) {
                Throttler::incrementLoginAttempts($name, $request);
            }
        });
    }
}