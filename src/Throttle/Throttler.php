<?php

namespace Infinity\Bastion\Throttle;

use Illuminate\Cache\RateLimiter;
use Illuminate\Http\Request;

class Throttler
{
    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param $identifier
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    public static function hasTooManyLoginAttempts($identifier, Request $request)
    {
        $rateLimiter = app(RateLimiter::class);

        if ($rateLimiter->tooManyAttempts(
            $identifier . $request->ip(),
            self::maxLoginAttempts(), self::lockoutTime() / 60
        )
        ) {
            return self::getLockoutErrorMessage($rateLimiter->availableIn(
                $identifier . $request->ip()
            ));
        }

        return false;
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param $identifier
     * @param  \Illuminate\Http\Request $request
     * @return int
     */
    public static function incrementLoginAttempts($identifier, Request $request)
    {
        app(RateLimiter::class)->hit($identifier . $request->ip());
    }

    /**
     * Get the login lockout error message.
     *
     * @param  int $seconds
     * @return string
     */
    protected static function getLockoutErrorMessage($seconds)
    {
        return \Lang::has('auth.throttle')
            ? \Lang::get('auth.throttle', ['seconds' => $seconds])
            : 'Too many login attempts. Please try again in ' . $seconds . ' seconds.';
    }

    /**
     * Clear the login locks for the given user credentials.
     *
     * @param $identifier
     * @param  \Illuminate\Http\Request $request
     */
    public static function clearLoginAttempts($identifier, Request $request)
    {
        app(RateLimiter::class)->clear($identifier . $request->ip());
    }

    /**
     * Get the maximum number of login attempts for delaying further attempts.
     *
     * @return int
     */
    public static function maxLoginAttempts()
    {
        return config('bastion.throttle.attempts', 5);
    }

    /**
     * The number of seconds to delay further login attempts.
     *
     * @return int
     */
    public static function lockoutTime()
    {
        return config('bastion.throttle.attempts', 60);
    }
}