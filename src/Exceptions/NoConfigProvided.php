<?php

namespace Infinity\Bastion\Exceptions;

/**
 * No config provided for Bastion auth manager.
 *
 * @link http://php.net/manual/en/class.exception.php
 */
class NoConfigProvided extends \Exception
{

}