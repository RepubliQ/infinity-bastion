<?php

namespace Infinity\Bastion\Exceptions;

/**
 * Invalid driver supplied for Bastion auth manager.
 *
 * @link http://php.net/manual/en/class.exception.php
 */
class InvalidDriverException extends \Exception
{

}