<?php

namespace Infinity\Bastion\Exceptions;


/**
 * Invalid attempt.
 * @link http://php.net/manual/en/class.exception.php
 */
class InvalidAttempt extends \Exception
{

}