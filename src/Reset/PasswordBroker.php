<?php

namespace Infinity\Bastion\Reset;

use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Mail\Message;
use Illuminate\Validation\Validator;
use Infinity\Bastion\Contracts\Reset\PasswordBroker as PasswordBrokerContract;
use Infinity\Bastion\Contracts\Reset\TokenRepository;
use Infinity\Bastion\Contracts\User\CanResetPassword;
use Infinity\Bastion\Contracts\User\UserRepository;

class PasswordBroker implements PasswordBrokerContract
{
    /**
     * User provider implementation.
     *
     * @type UserRepository
     */
    protected $users;

    /**
     * Token provider implementation.
     *
     * @type TokenRepository
     */
    protected $tokens;

    /**
     * The mailer instance.
     *
     * @type Mailer
     */
    protected $mailer;

    /**
     * Email view.
     *
     * @type string
     */
    protected $emailView;

    /**
     * Create a new password broker instance.
     *
     * @param UserRepository $users
     * @param Mailer $mailer
     * @param TokenRepository $tokens
     * @param $emailView
     */
    public function __construct(UserRepository $users, Mailer $mailer, TokenRepository $tokens, $emailView)
    {
        $this->users = $users;
        $this->mailer = $mailer;
        $this->tokens = $tokens;
        $this->emailView = $emailView;
    }

    /**
     * Send a password reset link to a user.
     *
     * @param CanResetPassword|string $user
     * @param \Closure|null $callback
     * @return string
     */
    public function sendResetLink($user, \Closure $callback = null)
    {
        if ($user === null) {
            return false;
        }

        if (!$user instanceof CanResetPassword) {
            $user = $this->users->findByIdentifier($user);
        }

        if ($user === null) {
            return false;
        }

        $token = $this->tokens->create($user);

        /** @noinspection PhpVoidFunctionResultUsedInspection */
        $this->mailer->send($this->emailView, compact('user', 'token'),
            function (Message $message) use ($user, $token, $callback) {
                $message->to($user->getEmailForPasswordReset());

                if ($callback !== null) {
                    $callback($message, $user, $token);
                }
            });

        return true;
    }

    /**
     * Reset the password for the given token.
     *
     * @param CanResetPassword|string $user
     * @param array $data
     * @param Validator|\Closure|null $validator
     * @return mixed
     */
    public function reset($user, array $data, $validator = null)
    {
        if ($user === null || empty($data['token'])) {
            return false;
        }

        if (!$user instanceof CanResetPassword) {
            $user = $this->users->findByIdentifier($user);
        }

        if ($user === null || !$this->validateReset($user, $data)) {
            return false;
        }

        if ($validator instanceof \Closure) {
            $valid = $validator($data);
        } elseif ($validator instanceof Validator) {
            $validator->setData($data);
            $valid = $validator->passes();
        } else {
            $valid = $this->validatePasswordWithDefaults($user, $data);
        }

        if ($valid) {
            $this->tokens->delete($data['token']);
            $user->updatePassword($data[$user->getUserPasswordName()]);
        }

        return $valid;
    }

    /**
     * Validate a password reset for the given credentials.
     *
     * @param CanResetPassword $user
     * @param array $data
     * @return CanResetPassword
     */
    protected function validateReset(CanResetPassword $user, array $data)
    {
        if (!$this->tokens->exists($user, $data['token'])) {
            return false;
        }

        return true;
    }

    /**
     * Determine if the passwords are valid for the request.
     *
     * @param CanResetPassword $user
     * @param array $data
     * @return bool
     */
    protected function validatePasswordWithDefaults(CanResetPassword $user, array $data)
    {
        $passwordName = $user->getUserPasswordName();
        $password = $data[$passwordName];
        $confirm = $data["{$passwordName}_confirmation"];

        return $password === $confirm && mb_strlen($password) >= 6;
    }
}