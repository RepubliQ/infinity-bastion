<?php

namespace Infinity\Bastion\Reset\Providers;

use Carbon\Carbon;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use Infinity\Bastion\Contracts\Reset\TokenRepository as TokenRepositoryContract;
use Infinity\Bastion\Contracts\User\CanResetPassword;

class DatabaseTokenRepository implements TokenRepositoryContract
{
    /**
     * The database connection instance.
     *
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * The token database table.
     *
     * @var string
     */
    protected $table;

    /**
     * The hashing key.
     *
     * @var string
     */
    protected $hashKey;

    /**
     * The number of seconds a token should last.
     *
     * @var int
     */
    protected $expires;

    /**
     * Create a new token repository instance.
     *
     * @param ConnectionInterface $connection
     * @param string $table
     * @param string $hashKey
     * @param int $expires
     */
    public function __construct(ConnectionInterface $connection, $table, $hashKey, $expires = 60)
    {
        $this->table = $table;
        $this->hashKey = $hashKey;
        $this->expires = $expires * 60;
        $this->connection = $connection;
    }

    /**
     * Create a new token.
     *
     * @param CanResetPassword $user
     * @return string
     */
    public function create(CanResetPassword $user)
    {
        $this->deleteExisting($user);

        $token = $this->createNewToken();

        $this->newQuery()->insert([
            'email' => $user->getEmailForPasswordReset(),
            'token' => $token,
            'model_type' => get_class($user),
            'created_at' => new Carbon(),
        ]);

        return $token;
    }

    /**
     * Determine if a token record exists and is valid.
     *
     * @param CanResetPassword $user
     * @param string $token
     * @return bool
     */
    public function exists(CanResetPassword $user, $token)
    {
        $email = $user->getEmailForPasswordReset();

        $token = (array)$this->newQuery($user)->where('email', $email)->where('model_type', get_class($user))->where('token', $token)->first();

        return $token && strtotime($token['created_at']) + $this->expires > time();
    }

    /**
     * Delete a token record.
     *
     * @param string $token
     */
    public function delete($token)
    {
        $this->newQuery()->where('token', $token)->delete();
    }

    /**
     * Delete expired tokens.
     */
    public function deleteExpired()
    {
        $this->newQuery()->where('created_at', '<', Carbon::now()->subSeconds($this->expires))->delete();
    }

    /**
     * Get the database connection instance.
     *
     * @return \Illuminate\Database\ConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Create a new token for the user.
     *
     * @return string
     */
    public function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), $this->hashKey);
    }

    /**
     * Delete all existing reset tokens from the database.
     *
     * @param CanResetPassword $user
     * @return int
     */
    protected function deleteExisting(CanResetPassword $user)
    {
        return $this->newQuery($user)->where('email', $user->getEmailForPasswordReset())->delete();
    }

    /**
     * Begin a new database query against the table.
     *
     * @param CanResetPassword|null $user
     * @return Builder
     */
    protected function newQuery(CanResetPassword $user = null)
    {
        if ($user === null) {
            return $this->connection->table($this->table);
        }

        return $this->connection->table($this->table)->where('model_type', get_class($user));
    }
}