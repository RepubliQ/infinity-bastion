<?php

namespace Infinity\Bastion\Reset;

use Illuminate\Support\ServiceProvider;
use Infinity\Bastion\Base\AuthManager;
use Infinity\Bastion\Base\Bastion;
use Infinity\Bastion\Reset\Providers\DatabaseTokenRepository;

class PasswordResetServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $app = $this->app;

        $this->app['auth']->macro('passwordBroker', function (Bastion $bastion) use ($app) {
            return $app['auth.password']->get($bastion->name());
        });
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->registerTokenRepository();
        $this->registerPasswordBroker();
    }

    /**
     * Register the password broker instance.
     */
    private function registerPasswordBroker()
    {
        $this->app->singleton('auth.password', function ($app) {
            /** @type AuthManager $bastion */
            $bastion = $app['auth'];

            $providers = [];

            foreach ($app['config']['bastion.auth'] as $type => $config) {
                $providers[$type]['provider'] = $bastion->getUserProvider($type, true);
                $providers[$type]['emailView'] = $config['email'];
            }

            return new PasswordBrokerManager($app['mailer'], $app['auth.password.tokens'], $providers);
        });
    }

    /**
     * Register the token repository implementation.
     */
    private function registerTokenRepository()
    {
        $this->app->singleton('auth.password.tokens', function ($app) {
            $connection = $app['db']->connection();
            $table = $app['config']['bastion.password.table'];
            $key = $app['config']['app.key'];
            $expire = $app['config']['bastion.password.expire'];

            return new DatabaseTokenRepository($connection, $table, $key, $expire);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['auth.password', 'auth.password.tokens'];
    }
}