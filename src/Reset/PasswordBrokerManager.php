<?php

namespace Infinity\Bastion\Reset;

use Illuminate\Contracts\Mail\Mailer;
use Infinity\Bastion\Contracts\Reset\TokenRepository;

class PasswordBrokerManager
{
    /**
     * Initiated password broker services.
     *
     * @type array
     */
    protected $brokers = [];

    /**
     * Deferred password broker configurations.
     *
     * @type array
     */
    protected $deferred = [];

    /**
     * PasswordBrokerManager constructor.
     *
     * @param Mailer $mailer
     * @param TokenRepository $tokens
     * @param array $providers
     */
    public function __construct(Mailer $mailer, TokenRepository $tokens, array $providers)
    {
        foreach ($providers as $type => $provider) {
            $this->deferred[$type] = [$provider['provider'], $mailer, $tokens, $provider['emailView']];
        }
    }

    /**
     * Get password broker for type.
     *
     * @param $type
     * @return mixed
     */
    public function get($type)
    {
        if (isset($this->brokers[$type])) {
            return $this->brokers[$type];
        }

        if (isset($this->deferred[$type])) {
            list($provider, $mailer, $tokens, $emailView) = $this->deferred[$type];

            $this->brokers[$type] = new PasswordBroker(value($provider), $mailer, $tokens, $emailView);

            return $this->brokers[$type];
        }

        return null;
    }
}